# -*- coding: utf-8 -*-
# RafKac

from django.test import TestCase

from repeta import Employee, Clients


class EmployeeModelClass(TestCase):

    def test_employee(self):
        p1 = Employee("Jan", "Kowalski", 2345)

        self.assertIs(p1.last, "Kowalski")
        self.assertIs(p1.first, "Jan")
        self.assertIs(p1.salary, 2345)

    def test_clients(self):
        p1 = Clients("Pablo", "Escobar", 2)

        self.assertIs(p1.first, "Pablo")
        self.assertIs(p1.last, "Escobar")
        self.assertIs(p1.status, 2)

