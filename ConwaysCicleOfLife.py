# -*- coding: utf-8 -*-
# RafKac
# 2021_09_23
#
# Cykl życia Conwaya:
# - jeśli komórka ma mniej niż 2 żywych sąsiadów, umiera z samotności
# - jeśli ma więcej niż 3 sąsiadów, umiera z przeludnienia
# - żywa z 2 lub 3 żywymi sąsiadami żyje dalej
# - martwa z 3 żywymi sąsiadami ożywa
#
# przy użyciu bibliotek pygame
# na podstawie python101

import pygame
import sys
import random
from pygame.locals import *


pygame.init()

szerokosc_okna = 800
wysokosc_okna = 600

okno_gry = pygame.display.set_mode((szerokosc_okna, wysokosc_okna), 0, 32)
pygame.display.set_caption("Gra o życie Conwaya")

rozmiar_komorki = 10
kom_poziom = int(szerokosc_okna / rozmiar_komorki)
kom_pion = int(wysokosc_okna / rozmiar_komorki)
martwa = 0
zywa = 1


pole_gry = [martwa] * kom_poziom
for i in range(kom_poziom):
    pole_gry[i] = [martwa] * kom_pion


def przygotuj_populacje(pole_gry):
    nastepna_gen = [martwa] * kom_poziom
    for i in range(kom_poziom):
        nastepna_gen[i] = [martwa] * kom_pion

    for y in range(kom_pion):
        for x in range(kom_poziom):
            populacja = 0
            try:
                if pole_gry[x - 1][y - 1] == zywa:
                    populacja += 1
            except IndexError:
                pass
            try:
                if pole_gry[x][y - 1] == zywa:
                    populacja += 1
            except IndexError:
                pass
            try:
                if pole_gry[x + 1][y - 1] == zywa:
                    populacja += 1
            except IndexError:
                pass
            try:
                if pole_gry[x - 1][y] == zywa:
                    populacja += 1
            except IndexError:
                pass
            try:
                if pole_gry[x + 1][y] == zywa:
                    populacja += 1
            except IndexError:
                pass
            try:
                if pole_gry[x - 1][y + 1] == zywa:
                    populacja += 1
            except IndexError:
                pass
            try:
                if pole_gry[x][y + 1] == zywa:
                    populacja += 1
            except IndexError:
                pass
            try:
                if pole_gry[x + 1][y + 1] == zywa:
                    populacja += 1
            except IndexError:
                pass

            if pole_gry[x][y] == zywa and (populacja < 2 or populacja > 3):
                nastepna_gen[x][y] = martwa
            elif pole_gry[x][y] == zywa and (2 <= populacja <= 3):
                nastepna_gen[x][y] = zywa
            elif pole_gry[x][y] == martwa and populacja == 3:
                nastepna_gen[x][y] = zywa

    return nastepna_gen


def rysuj_populacje():
    for y in range(kom_pion):
        for x in range(kom_poziom):
            if pole_gry[x][y] == zywa:
                pygame.draw.rect(okno_gry, (255, 255, 255), Rect((x * rozmiar_komorki, y * rozmiar_komorki), (rozmiar_komorki, rozmiar_komorki)), 1)


def main():
    global pole_gry, okno_gry

    zycie_trwa = False
    przycisk_w_dol = False
    petla = True

    while petla:
        for event in pygame.event.get():
            if event.type == QUIT:
                petla = False

            if event.type == KEYDOWN and event.key == K_RETURN:
                zycie_trwa = True

            if event.type == KEYDOWN and event.key == K_SPACE:
                zycie_trwa = False

            if zycie_trwa is False:
                if event.type == MOUSEBUTTONDOWN:
                    przycisk_w_dol = True
                    przycisk_typ = event.button

                if event.type == MOUSEBUTTONUP:
                    przycisk_w_dol = False

                if przycisk_w_dol:
                    mouse_x, mouse_y = pygame.mouse.get_pos()
                    mouse_x = int(mouse_x / rozmiar_komorki)
                    mouse_y = int(mouse_y / rozmiar_komorki)

                    if przycisk_typ == 1:
                        pole_gry[mouse_x][mouse_y] = zywa

                    if przycisk_typ == 3:
                        pole_gry[mouse_x][mouse_y] = martwa

        if zycie_trwa is True:
            pole_gry = przygotuj_populacje(pole_gry)

        okno_gry.fill((0, 0, 0))
        rysuj_populacje()

        pygame.display.update()
        pygame.time.delay(100)


if __name__ == '__main__':
    main()


