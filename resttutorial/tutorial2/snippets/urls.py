# -*- coding: utf-8 -*-


from django.urls import path, include
from django.views.generic import TemplateView

from snippets import views
from rest_framework.routers import DefaultRouter
# from rest_framework.urlpatterns import format_suffix_patterns
# from rest_framework import renderers

# from snippets.views import SnippetViewSet, UserViewSet, api_root

"""
# pierwsza wersja - dla widoków będących metodami

urlpatterns = [
    path('snippets/', views.snippet_list),
    path('snippets/<int:pk>/', views.snippet_detail)
]

urlpatterns = format_suffix_patterns(urlpatterns)
"""

"""
# druga wersja - dla class-based views
urlpatterns = [
    path('snippets/', views.SnippetList.as_view(), name='snippet-list'),
    path('', views.api_root),
    path('snippets/<int:pk>/highlight/', views.SnippetHighlight.as_view(), name="snippet-highlight"),
    path('snippets/<int:pk>', views.SnippetDetail.as_view(), name='snippet-detail'),
    path('users/', views.UserList.as_view(), name="user-list"),
    path('users/<int:pk>', views.UserDetail.as_view(), name='user-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
"""

'''
# trzecia wersja - dla dla ViewSets
snippet_list = SnippetViewSet.as_view({
    'get': 'list',
    'post': 'create',
})

snippet_detail = SnippetViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

snippet_highlight = SnippetViewSet.as_view({
    'get': 'highlight'
}, render_classes=[renderers.StaticHTMLRenderer])

user_list = UserViewSet.as_view({
    'get': 'list'
})

user_detail = UserViewSet.as_view({
    'get': 'retrieve'
})

urlpatterns = format_suffix_patterns([
    path('', api_root),
    path('snippets/', snippet_list, name='snippet_list'),
    path('snippets/<int:pk>/', snippet_detail, name='snippet_detail'),
    path('snippets/<int:pk>/highlight/', snippet_highlight, name='snippet_highlight'),
    path('users/', user_list, name='user_list'),
    path('users/<int:pk>/', user_detail, name='user_detail'),
])'''


# czwarta wersja - z użyciem klasy Routes
# tworzymy router i rejestrujemy na nim nasze widoki
router = DefaultRouter()
router.register(r'snippets', views.SnippetViewSet)
router.register(r'users', views.UserViewSet)

# URLe dla API są teraz determinowane automatycznie przez router
urlpatterns = [
    path('', include(router.urls)),
    # dodajemy TemplateView dla dokumentacji Swagger-UI:
    path('swagger-ui/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),
]
