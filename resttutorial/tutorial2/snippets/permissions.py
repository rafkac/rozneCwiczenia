# -*- coding: utf-8 -*-

from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Uprawnienia pozwalające tylko właścicelom obiektów na ich edycję.
    """

    def has_object_permission(self, request, view, obj):
        # zawsze dopuszczamy zapytania GET, HEAD, OPTIONS, czyli czytanie
        if request.method in permissions.SAFE_METHODS:
            return True

        # uprawnienie pisania tylko właścicielowi
        return obj.owner == request.user

