# -*- coding: utf-8 -*-
# RafKac
# 2021_09_22
# na podstawie kodu ze strony:
# https://python101.readthedocs.io/pl/latest/podstawy/mlotek/index.html
# program służy przybliżeniu korzystania z bibliotek pylab, numpy oraz matplotlib

import random
import pylab
import numpy as np
import matplotlib.pyplot as plt


def maly_lotek():
    liczba = random.randint(1, 10)
    czyJeszcze = True;
    while czyJeszcze:
        odp = input("Jaką liczbę od 1 do 10 mam na myśli? ")
        print("podałeś liczbę: " + odp)
        if int(odp) == liczba:
            czyJeszcze = False
        else:
            print("Nie zgadłeś, próbuj jeszcze raz.")
    print("Wylosowana liczba: ", liczba)


def duzy_lotek():
    czy_jeszcze = True
    tab = []
    wylosowane = []

    try:
        ile_liczb = int(input("ile liczb chcesz odgadnąć? "))
        maks_liczba = int(input("Maksymala losowana liczba? "))
        print("wytypuj {} liczb z {}".format(ile_liczb, maks_liczba))
    except ValueError:
        print("Błędne dane!")
        exit()

    while czy_jeszcze:
        try:
            i = int(input())
            if i > maks_liczba or i < 0:
                print("przekroczono zakres, jeszcze raz!")
            elif tab.count(i) > 0:
                print("Liczba już występuje, jeszcze raz! ")
            else:
                tab.append(i)
                print(tab)
        except ValueError:
            print("Zły typ danych!")
            continue

        if len(tab) == ile_liczb:
            czy_jeszcze = False
    licz = 0
    while licz < ile_liczb:
        liczba = random.randint(1, maks_liczba)
        if wylosowane.count(liczba) == 0:
            wylosowane.append(liczba)
            licz += 1

    tab = set(tab)
    wylosowane = set(wylosowane)
    print("Wylosowane liczby: ", wylosowane)
    trafione = tab & wylosowane
    if trafione:
        print("Trafiono: {}, są to liczby: {}".format(len(trafione), trafione))
    else:
        print("Zero trafień - spróbuj jeszcze raz.")


def matplotlib_przyklad():
    czy_jeszcze = True

    while czy_jeszcze:
        try:
            a = int(input("Podaj współczynnik a: "))
            b = int(input("Podaj współczynnik b: "))
        except ValueError:
            print("Zły format danych - jeszcze raz!")
            continue
        czy_jeszcze = False

    x = range(-10, 11)
    y = []

    y = [a * i + b for i in x]

    pylab.plot(x, y)
    pylab.title("Wykres f(x) = {} * x - {}".format(a, b))
    pylab.grid(True)
    pylab.show()


def dwie_funkcje():
    """
    f(x) = x/(-3) + a dla x <= 0
    f(x) = x*x/3 dla x >= 0
    """
    krok = 0.5
    nadal = True

    while nadal:
        try:
            a = int(input("Podaj współczynnik a: "))
        except ValueError:
            print("Zły typ danych, jeszcze raz!")
            continue
        nadal = False

    x = pylab.arange(-10, 10.5, krok)
    y1 = [i / (-3) + a for i in x if i <= 0]
    y2 = [i*i/3 for i in x if i >= 0]

    pylab.plot(x[:len(y1)], y1)
    pylab.plot(x[-len(y2):], y2)

    pylab.title("Wykres f(x)")
    pylab.grid(True)
    pylab.show()


def ruchy_Browna():
    """
    są to chaotyczne ruchy cząsteczek na płaszczyźnie dwuwymiarowej
    założenia:
    - śledzona cząsteczka znajduje się w początku układu współrzędnych (0, 0)
    - w każdym ruchu cząsteczka przemieszcza się o stały wektor o wartości 1
    - kierunek ruchu wyznaczać będziemy losując kąt z zakresu <0, 2Pi>
    - współrzędne kolejnego położenia cząsteczki wyliczać będziemy ze wzorów:
            x_n = x_(n-1) + r * cos(fi)
            y_n = y_(n-1) + r * sin(fi)
        gdzie r - długość jednego kroku
        fi - kąt wskazujący kierunek ruchu w odniesieniu do osi OX
    - końcowy wektor przesunięcia obliczymy ze wzoru:
            |s| = sqrt(x^2 + y^2)
    """
    lx = [0]
    ly = [0]
    x = y = 0
    ss = 0

    try:
        n = int(input("Ile ruchów? "))
    except ValueError:
        print("Zły format danych!")

    for i in range(0, n):
        rad = float(random.randint(0, 360)) * np.pi / 180
        x = x + np.cos(rad)
        y = y + np.sin(rad)
        lx.append(x)
        ly.append(y)

    print(lx, ly)
    print("Wektor końcowego przesunięcia: ")
    s = np.sqrt(x**2 + y**2)
    print(s)
    print("Drugi sposób: ")
    s = np.fabs(np.sqrt(x**2 + y**2))
    print(s)
    print("Końcowy wektor przesunięcia: ")
    i = 0
    while i < n:
        ss += np.sqrt(lx[i]**2 + ly[i]**2)
        i += 1
    print(ss)

    plt.plot(lx, ly, "o:", color="green", linewidth=2, alpha=0.5)
    plt.legend(["Dane x, y\nPrzemieszczenie: " + str(s)], loc="upper left")
    plt.xlabel("lx")
    plt.ylabel("ly")
    plt.title("Ruchy Browna")
    plt.grid(True)
    plt.show()


def funkcja_kwadratowa():
    a = 1
    b = -3
    c = 1
    krok = 1
    x = pylab.arange(-10, 11, krok)
    y = [a * i**2 + b * i + c for i in x]

    plt.plot(x, y, color="red", linewidth=3, alpha=0.5)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("wykres f(x) = a*x*x + b*x + c")
    plt.grid(True)
    plt.show()


def main():
    #maly_lotek()
    #duzy_lotek()
    #matplotlib_przyklad()
    #dwie_funkcje()
    #ruchy_Browna()
    funkcja_kwadratowa()


if __name__ == '__main__':
    main()
