# -*- coding: utf-8 -*-
# RafKac
# 2021_09_27
# na podstawie https://www.youtube.com/watch?v=HJVukCV6orc
import pytest


class Bank:
    """
    Przygotować klasę Bank. Powinna mieć możliwość wpłacania i wypłacania.
    W chwili inwestycji bank powinien zawierać puste saldo.

    Nie testuj sytuacji, w której wypłacasz więcej, niż to możliwe.

    Następnie rozwiń klasę tak, aby nie można było wypłacić więcej, niż jest w kasie.
    W takim przypadku program powinien wyrzucić wyjątek.
    """

    def __init__(self):
        self.amount = 0

    def add_money(self, money: int):
        self.amount += money

    def withdraw_money(self, money: int):
        if money > self.amount:
            raise NotEnoughCash("Nie masz tyle pieniędzy!!!")
        self.amount -= money
        return money


class NotEnoughCash(Exception):
    pass


class TestBank:
    def test_create_bank(self):
        bank = Bank()
        assert bank.amount == 0
        assert isinstance(bank, Bank)

    def test_add_money(self):
        # given
        bank = Bank()

        # then
        bank.add_money(100)

        # then
        assert bank.amount == 100

    def test_add_money_twice(self):
        # given
        bank = Bank()

        # when
        bank.add_money(100)
        bank.add_money(100)

        # then
        assert bank.amount == 200
        
    def test_withdraw_money(self):
        # given
        bank = Bank()
        bank.add_money(100)

        # when
        money = bank.withdraw_money(100)

        # then
        assert money == 100
        assert bank.amount == 0

    def test_withdraw_over_amount(self):
        with pytest.raises(NotEnoughCash):
            bank = Bank()
            bank.withdraw_money(200)

