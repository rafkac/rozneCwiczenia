# -*- coding: utf-8 -*-
# RafKac
# 2021_09_27
# na podstawie https://www.youtube.com/watch?v=HJVukCV6orc


def is_adult(age: int) -> bool:
    """
    przygotuj funkcje sprawdzającą, czy osoba o przekazanym wieku jest pełnoletnia
    :return:
    """
    if age >= 18:
        return True
    else:
        return False


def test_is_adult():
    # given - wszystko, co mamy, co będzie służyło do przeprowadzenia testu
    age = 18

    # when - to, co się wydarza
    result = is_adult(age)

    # then
    assert result
    assert is_adult(20)


def test_is_not_adult():
    assert not is_adult(17)
    assert not is_adult(3)


def triangle(a: int, h: int) -> float:
    """
    na podstawie wysokości oraz długości podstawy wyświetli (print) pole trójkąta.
    :return:
    """
    field = 0.5 * a * h
    print(field)


def test_triangle(capsys):
    # given
    base = 10
    height = 8

    # when
    field = triangle(base, height)
    out, err = capsys.readouterr()

    # then
    # assert field == 40
    assert out == '40.0\n'


def main():
    pass


if __name__ == '__main__':
    main()
