# -*- coding: utf-8 -*-
# czat/models.py

from django.db import models
from django.contrib.auth.models import User


class Wiadomosc(models.Model):

    tekst = models.CharField("treść wiadomości", max_length=250)
    data_pub = models.DateTimeField("data publikacji", auto_now_add=True)
    autor = models.ForeignKey(User, on_delete=models.CASCADE,)

    class Meta:
        verbose_name = u"wiadomość"
        verbose_name_plural = u"wiadomości"
        ordering = ['data_pub']

    def __str__(self):
        return self.tekst
