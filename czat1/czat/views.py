# -*- coding: utf-8 -*-
# czat/views.py

from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.urls import reverse
from czat.models import Wiadomosc


def index(request):
    #return HttpResponse("Witaj w aplikacji czat!")
    return render(request, 'index.html')


def loguj(request):
    if request.method == "POST":
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            login(request, form.get_user())
            messages.success(request, "Zostałeś zalogowany!")
            return redirect(reverse('czat:index'))

    kontekst = {'form': AuthenticationForm()}
    return render(request, 'loguj.html', kontekst)


def wyloguj(request):
    logout(request)
    messages.info(request, "Zostałeś wylogowany!")
    return redirect(reverse('czat:index'))


def wiadomosci(request):
    if request.method == "POST":
        tekst = request.POST.get('tekst', '')
        if not 0 < len(tekst) <= 250:
            messages.error(request, "Wiadomość nie może być pusta, może mieć maksymalnie 250 znaków!")
        else:
            wiadomosc = Wiadomosc(
                tekst=tekst,
                autor=request.user
            )
            wiadomosc.save()
            return redirect(reverse('czat:wiadomosci'))

    wiadomosci = Wiadomosc.objects.all()
    kontekst = {'wiadomosci': wiadomosci}
    return render(request, 'wiadomosci.html', kontekst)
