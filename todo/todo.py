# -*- coding: utf-8 -*-
# RafKac
# 2021_09_28
# na podstawie kodu udostępnionego na stronie python101.readthedocs.io
# dorobić:
# usuwanie z bazy               v
# mechanizm logowania użytkownika tak, aby użytkownik mógł dodawać i edytować tylko swoją listę zadań

from flask import Flask, render_template
from flask import g
import os
import sqlite3
from datetime import datetime
from flask import flash, redirect, url_for, request


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


app.config.update(dict(
    SECRET_KEY="bardzosekretnawartosc",
    DATABASE=os.path.join(app.root_path, 'db.sqlite'),
    SITE_NAME="Moje zadania"
))


def get_db():
    """funkcja tworząca połączenie z bazą danych"""
    if not g.get('db'):
        con = sqlite3.connect(app.config['DATABASE'])
        con.row_factory = sqlite3.Row
        g.db = con
    return g.db


@app.teardown_appcontext
def close_db(error):
    """Zamykanie połączenia z bazą"""
    if g.get('db'):
        g.db.close()


@app.route('/zadania', methods=['GET', 'POST'])
def zadania():
    error = None
    if request.method == 'POST':
        zadanie = request.form['zadanie'].strip()
        if len(zadanie) > 0:
            zrobione = '0'
            data_pub = datetime.now()
            db = get_db()
            db.execute('INSERT INTO zadania VALUES (?, ?, ?, ?);',
                       [None, zadanie, zrobione, data_pub])
            db.commit()
            flash("Dodano nowe zadanie.")
            return redirect(url_for('zadania'))

        error = "Nie możesz dodać pustego zadania!"

    db = get_db()
    kursor = db.execute('SELECT * FROM zadania ORDER BY data_pub DESC;')
    zadania = kursor.fetchall()
    return render_template('zadania_lista.html', zadania=zadania, error=error)


@app.route('/zrobione', methods=['POST'])
def zrobione():
    """Zmiana statusu zadania na wykonane."""
    zadanie_id = request.form['id']
    db = get_db()
    db.execute('UPDATE zadania SET zrobione=1 WHERE id=?', [zadanie_id])
    db.commit()
    flash("Zmieniono status zadania.")
    return redirect(url_for('zadania'))


@app.route("/usuniete", methods=['POST'])
def usuniete():
    """ metoda usuwająca zadanie, moja własna"""
    zadanie_id = request.form['id']
    db = get_db()
    db.execute("DELETE FROM zadania WHERE id=?", [zadanie_id])
    db.commit()
    flash("Usunięto zadanie: {}".format(zadanie_id))
    return redirect(url_for("zadania"))


if __name__ == '__main__':
    app.run(debug=True)
