# -*- coding: utf-8 -*-
# quiz-orm/main.py

from app import app, baza
from views import *
import os
from models import *
from dane import *


if __name__ == '__main__':
    if not os.path.exists(app.config['DATABASE']):
        baza.create_tables([Pytanie, Odpowiedz], safe=True)
        dodaj_pytania(pobierz_dane("pytania.csv"))
    app.run(debug=True)
