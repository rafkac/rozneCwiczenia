# -*- coding: utf-8 -*-
# RafKac
# 2021_09_28
# na podstawie kursu ze strony python101.readthedocs.io
#
# Aplikacja internetowa Quiz w oparciu o Flask 0.12

from flask import Flask, render_template
from flask import request, redirect, url_for, flash


app = Flask(__name__)


app.config.update(dict(SECRET_KEY='bardzosekretnawartosc',))


DANE = [{
    'pytanie': "Stolica Hiszpani to:",
    'odpowiedzi': ['Madryt', 'Warszawa', 'Barcelona'],
    'odpok': 'Madryt'},
    {
    'pytanie': "Objętość sześcianu o boku 6 cm wynosi:",
    'odpowiedzi': ['36', '216', '18'],
    'odpok': '216'},
    {
    'pytanie': "Symbol pierwiastka Helu to:",
     "odpowiedzi": ['Fe', "H", "He"],
     'odpok': 'He'},
]


@app.route('/', methods=['GET', 'POST'])
def index():

    if request.method == "POST":
        punkty = 0
        odpowiedzi = request.form

        for pnr, odp in odpowiedzi.items():
            if odp == DANE[int(pnr)]['odpok']:
                punkty += 1

        flash("Liczba poprawnych odpowiedzi to: {0}".format(punkty))
        return redirect(url_for('index'))

    return render_template('index.html', pytania=DANE)


if __name__ == "__main__":
    app.run(debug=True)