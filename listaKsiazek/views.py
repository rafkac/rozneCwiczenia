# -*- coding: utf-8 -*-
# listaKsiazek/views.py

import sqlite3

from flask import render_template, flash, url_for, redirect, request, abort
from app import app
from dane import get_db, db_local
from models import Book
from forms import *


def query():
    """metoda zwracająca całą listę książek"""
    db = get_db()
    kursor = db.execute('SELECT * FROM books;')
    books = kursor.fetchall()
    return render_template("index.html", books_data=books)


def query_details():
    con = sqlite3.connect(db_local)
    c = con.cursor()
    c.execute("""SELECT * FROM books """)
    book_data = c.fetchall()
    con.close()

    return book_data


@app.route('/')
def index():
    books_data = Book().select()

    if not books_data.count:
        flash("Brak książek w bazie.")
        return redirect(url_for('add_book'))

    return render_template('index.html', books_data=books_data)


@app.route('/add_book')
def add_book():
    pass
