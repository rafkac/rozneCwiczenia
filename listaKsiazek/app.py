# -*- coding: utf-8 -*-
# listaKsiazek/app.py
import os
from flask import Flask, g
from peewee import SqliteDatabase

app = Flask(__name__)

# konfiguracja aplikacji
app.config.update(dict(
    SECRET_KEY='bardzosekretnawartosc',
    TYTUL='Lista książek',
    DATABASE=os.path.join(app.root_path, 'books.db')
))


baza = SqliteDatabase(app.config['DATABASE'])


@app.before_request
def before_request():
    g.db = baza
    g.db.connection()


@app.after_request
def after_request(response):
    g.db.close()
    return response
