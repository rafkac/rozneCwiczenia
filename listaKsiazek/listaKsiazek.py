# -*- coding: utf-8 -*-
# RafKac
# 2021_09_30
# zadanie rekrutacyjne do firmy stx Next

from os import abort
from flask import Flask, render_template, request, redirect, g, flash, url_for
import sqlite3


app = Flask(__name__)
db_local = "books.db"
connection = sqlite3.connect(db_local)


app.config.update(dict(
    SECRET_KEY="sekretnykluczbotowielkatajemnica0987654321"
))


def create_database():
    global connection

    c = connection.cursor()
    c.execute("""DROP TABLE IF EXISTS books;""")
    c.execute("""
        CREATE TABLE books(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT not null,
            author text not null,
            date_pub date not null,
            ISBN text not null,
            pages int, 
            cover text,
            language text
        );
    """)

    connection.commit()
    connection.close()


def populate_database():
    global connection

    c = connection.cursor()
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Hobbit czyli Tam i z powrotem", "John Ronald Reuel Tolkien", "2014-01", "9788324403875", 315, 
            "https://books.google.pl/books?id=rToaogEACAAJ&dq=Hobbit&hl=&cd=2&source=gbs_api", "PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Sługa krwi", "Adam Przechrzta", "2021-05-31", "9788379646463", 448, 
            "https://bigimg.taniaksiazka.pl/images/popups/46A/9788379646463.jpg", "PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Jak być sobą", "Cath Howe", "2021-09-15", "9788328088054", 256, 
            "https://bigimg.taniaksiazka.pl/images/popups/A3C/9788328088054.jpg", "PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Białe noce", "Adam Przechrzta", "2019", "9788379641543", 422, 
            "https://bigimg.taniaksiazka.pl/images/popups/81F/9788379641543.jpg", "PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Winnetou", "Karol May", "2017", "9788377917237", 634, 
            "https://bigimg.taniaksiazka.pl/images/popups/D19/9788377917237.jpg" ,"PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Czlowiek, który był czwartkiem", "Gilbert Keith Chesterton", "2019", 
            "9788381167826", 232, "https://bigimg.taniaksiazka.pl/images/popups/2AB/9788381167826.jpg" ,"PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Polowanie na czerwony październik", "Tom Clancy", "2019", 
            "9788324057016", 560, "https://bigimg.taniaksiazka.pl/images/popups/694/9788324057016.jpg" ,"PL");
    """)

    connection.commit()
    connection.close()


def query_database():
    global connection

    c = connection.cursor()
    c.execute("""
        SELECT * FROM books;
    """)

    books_info = c.fetchall()
    for book in books_info:
        print(book)

    connection.commit()
    connection.close()


def get_db():
    """ metoda tworząca połączenie z bazą danych """
    if not g.get('db'):
        con = sqlite3.connect(db_local)
        con.row_factory = sqlite3.Row
        g.db = con
    return g.db


@app.teardown_appcontext
def close_db(error):
    if g.get('db'):
        g.db.close()


@app.route('/', methods=['GET', 'POST'])
def index():
    error = None
    """if request.method == 'POST':
        title = request.form['title'].strip()
        author = request.form['author'].strip()
        date_pub = request.form['date_pub'].strip()
        isbn = request.form['ISBN'].strip()
        pages = request.form['pages'].strip()
        cover = request.form['cover'].strip()
        lang = request.form['language'].strip()
        if len(title) > 0 and len(author) > 0 and len(date_pub) > 0 and len(isbn) > 0:
            db = get_db()
            db.execute("INSERT INTO books VALUES (?, ?, ?, ?, ?, ?, ?, ?);",
                       [None, title, author, date_pub, isbn, pages, cover, lang])
            db.commit()
            flash("Dodano książkę.")
            books_data = query_date()
            return redirect(url_for('index'))
        error = "Nie możesz dodać pustej książki (bez autora, tytułu, day i ISBN)!"
    """
    books_data = query_details()
    return render_template('index.html', books_data=books_data, error=error)


def query():
    """metoda zwracająca całą listę książek"""
    db = get_db()
    kursor = db.execute('SELECT * FROM books;')
    books = kursor.fetchall()
    return render_template("index.html", books_data=books)


def query_date():
    """metoda pilnująca zakresu daty"""
    db = get_db()
    kursor = db.execute('SELECT * FROM books WHERE date_pub between $d1 and $d2;')
    books = kursor.fetchall()
    return render_template("index.html", books_data=books)


def get_or_404(pid):
    try:
        db = get_db()
        p = db.execute('SELECT * FROM books WHERE id=?;', [pid])
        books = p.fetchall()
        return books
    except p.close():
        abort(404)


@app.route('/edit_db/<pid>', methods=['GET', 'POST'])
def edit_db(pid):
    error = None
    if request.method == 'POST':
        title = request.form['title'].strip()
        author = request.form['author'].strip()
        date_pub = request.form['date_pub'].strip()
        isbn = request.form['ISBN'].strip()
        pages = request.form['pages'].strip()
        cover = request.form['cover'].strip()
        lang = request.form['language'].strip()
        if len(title) > 0 and len(author) > 0 and len(date_pub) > 0 and len(isbn) > 0:
            db = get_db()
            db.execute("UPDATE books SET (?, ?, ?, ?, ?, ?, ?, ?) WHERE id=?;",
                       [None, title, author, date_pub, isbn, pages, cover, lang, pid])
            db.commit()
            flash("Zmieniono książkę.")
            return redirect(url_for('index'))
        error = "Nie możesz dodać pustej książki, musi zawierać conajmniej tytuł, autora, datę i ISBN."
    books_data = query()
    return render_template('index.html', books_data=books_data, error=error)


@app.route('/save/<pid>', methods=['GET', 'POST'])
def save(pid):
    pass


def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            if type(error) is list:
                error = error[0]
            flash("Błąd: {}. Pole: {}".format(error, getattr(form, field).label.text))


@app.route('/add_book', methods=['GET', 'POST'])
def add_book():
    """form = DodajForm()
    if form.validate_on_submit():
        title = form.title.data
        author = form.author.data
        date_pub = form.date_pub.data
        isbn = form.isbn.data
        pages = form.pages.data
        cover = form.cover.data
        lang = form.language.data

        p = NewBook(title=title, author=author, date_pub=date_pub, isbn=isbn, pages=pages, cover=cover, lang=lang)
        p.save()

        flash("Dodano książkę {}.".format(title))
        return redirect((url_for("index")))
    elif request.method == "POST":
        flash_errors(form)
    return render_template("add_book.html", form=form)"""
    error = None
    if request.method == 'POST':
        title = request.form['title'].strip()
        author = request.form['author'].strip()
        date_pub = request.form['date_pub'].strip()
        isbn = request.form['ISBN'].strip()
        pages = request.form['pages'].strip()
        cover = request.form['cover'].strip()
        lang = request.form['language'].strip()
        if len(title) > 0 and len(author) > 0 and len(date_pub) > 0 and len(isbn) > 0:
            db = get_db()
            db.execute("INSERT INTO books VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                      [None, title, author, date_pub, isbn, pages, cover, lang])
            db.commit()
            flash("Dodano książkę.")
            return redirect('index.html')
        error = "Nie możesz dodać pustej książki (bez autora, tytułu, day i ISBN)!"

    books_data = query()
    return render_template('index.html', books_data=books_data, error=error)


@app.route('/delete/<pid>', methods=['GET', 'POST'])
def delete(pid):
    error = None
    if request.method == 'POST':
        book_id = request.form["book.id"].strip()
        print("Book id: " + book_id)
        print("PID: " + pid)
        db = get_db()
        db.execute("DELETE FROM books WHERE id=?", [pid])
        db.commit()
        flash("Usunięto zadanie: {}".format(book_id))
        return redirect(url_for('index'))

    books_data = query()
    return render_template('index.html', books_data=books_data)


def query_details():
    con = sqlite3.connect(db_local)
    c = con.cursor()
    c.execute("""
        SELECT * FROM books
    """)
    book_data = c.fetchall()
    con.close()

    return book_data


if __name__ == '__main__':
    query_database()
    app.run(debug=True)
