# -*- coding: utf-8 -*-
# listaKsiazek/models.py

from peewee import *
from app import baza


class BaseModel(Model):
    class Meta:
        database = baza


class Book(BaseModel):
    title = CharField()
    author = CharField()
    date_pub = DateField()
    isbn = CharField()
    pages = IntegerField()
    cover = CharField()
    language = CharField()

    def __str__(self):
        return self.title + " " + self.author + " " + self.date_pub + " " + self.isbn + " " \
               + self.pages + " " + self.cover + " " + self.language
