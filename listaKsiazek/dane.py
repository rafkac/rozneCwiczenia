# -*- coding: utf-8 -*-
# listaKsiazek/dane.py

import sqlite3
from flask import g

from app import app
from models import Book


db_local = "books.db"
connection = sqlite3.connect(db_local)


def pobierz_dane(link_zewnetrzny):
    dane = []
    return tuple(dane)


def dodaj_pytanie(dane):
    for title, author, date_pub, isbn, pages, cover, language in dane:
        p = Book(title=title, author=author, date_pub=date_pub, isbn=isbn, pages=pages,
                 cover=cover, language=language)
        p.save()
    print("Dodano książki z linku.")


def create_database():
    global connection

    c = connection.cursor()
    c.execute("""DROP TABLE IF EXISTS books;""")
    c.execute("""
        CREATE TABLE books(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT not null,
            author text not null,
            date_pub date not null,
            ISBN text not null,
            pages int, 
            cover text,
            language text
        );
    """)

    connection.commit()
    connection.close()


def populate_database():
    global connection

    c = connection.cursor()
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Hobbit czyli Tam i z powrotem", "John Ronald Reuel Tolkien", "2014-01", "9788324403875", 315, 
            "https://books.google.pl/books?id=rToaogEACAAJ&dq=Hobbit&hl=&cd=2&source=gbs_api", "PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Sługa krwi", "Adam Przechrzta", "2021-05-31", "9788379646463", 448, 
            "https://bigimg.taniaksiazka.pl/images/popups/46A/9788379646463.jpg", "PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Jak być sobą", "Cath Howe", "2021-09-15", "9788328088054", 256, 
            "https://bigimg.taniaksiazka.pl/images/popups/A3C/9788328088054.jpg", "PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Białe noce", "Adam Przechrzta", "2019", "9788379641543", 422, 
            "https://bigimg.taniaksiazka.pl/images/popups/81F/9788379641543.jpg", "PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Winnetou", "Karol May", "2017", "9788377917237", 634, 
            "https://bigimg.taniaksiazka.pl/images/popups/D19/9788377917237.jpg" ,"PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Czlowiek, który był czwartkiem", "Gilbert Keith Chesterton", "2019", 
            "9788381167826", 232, "https://bigimg.taniaksiazka.pl/images/popups/2AB/9788381167826.jpg" ,"PL");
    """)
    c.execute("""
        INSERT INTO books (id, title, author, date_pub, ISBN, pages, cover, language)
        VALUES (null, "Polowanie na czerwony październik", "Tom Clancy", "2019", 
            "9788324057016", 560, "https://bigimg.taniaksiazka.pl/images/popups/694/9788324057016.jpg" ,"PL");
    """)

    connection.commit()
    connection.close()


def get_db():
    """ metoda tworząca połączenie z bazą danych """
    if not g.get('db'):
        con = sqlite3.connect(db_local)
        con.row_factory = sqlite3.Row
        g.db = con
    return g.db


@app.teardown_appcontext
def close_db(error):
    if g.get('db'):
        g.db.close()