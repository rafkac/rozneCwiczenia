# -*- coding: utf-8 -*-
# listaKsiazek/forms.py

from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, IntegerField, DateField, validators
from wtforms.validators import Required


blad1 = "To pole jest wymagane"
blad2 = "zły format daty"
blad3 = "zły format danych"


class DodajForm(FlaskForm):
    title = StringField("Tytuł: ", validators=[Required(message=blad1), ])
    author = StringField("Autor: ", validators=[Required(message=blad1)])
    date_pub = DateField("Data publikacji: ", validators=[Required(message=blad1),
                                                          validators.DataRequired(message=blad2)])
    isbn = StringField("ISBN: ", validators=[Required(message=blad1)])
    pages = IntegerField("Liczba stron: ", validators=[validators.Length(min=1, max=5),
                                                       validators.Regexp(regex="[1-9][0-9]{0,4}", message=blad3)])
    cover = StringField("Okładka: ")
    language = StringField("Język: ")

    book_id = HiddenField('Id')
