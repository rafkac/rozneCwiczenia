# -*- coding: utf-8 -*-
# listaKsiazek/main.py

from app import app, baza
from views import *
from dane import *
from models import *
import os


if __name__ == '__main__':
    if not os.path.exists(app.config['DATABASE']):
        baza.create_tables(Book, True)
        populate_database()
    app.run(debug=True)
