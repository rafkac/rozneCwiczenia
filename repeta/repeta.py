# -*- coding: utf-8 -*-
# RafKac

"""
from django.db import models


class Person(models.Model):

    def __init__(self, first, last, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.first = first
        self.last = last

    def get_full_name(self):
        pass


class Employee(Person):

    def __init__(self, first, last, salary):
        super().__init__(first, last)
        self.salary = salary

    def get_full_name(self):
        return self.first + " " + self.last + " " + self.salary

    def diff(self):
        print("Klasa Emlpoyee")


class Clients(Person):
    def __init__(self, first, last, status):
        super().__init__(first, last)
        self.status = status

    def get_full_name(self):
        return self.first + " " + self.last + self.status

    def name(self):
        print("Status klienta: ")
        if self.status == 0:
            print("0 - in progress")
        elif self.status == 1:
            print("1 - execute")
        elif self.status == 2:
            print("2 - waiting")
        elif self.status == 3:
            print("3 - bad")
        else:
            print("It was mistake!")
"""


class Car:
    def __init__(self, color, mileage):
        self.color = color
        self.mileage = mileage

    def __repr__(self):
        return repr(self)

    def __str__(self):
        return self.color + " - " + str(self.mileage)
    

if __name__ == "__main__":
    print(__name__)

    a = 10
    x = 3

    def funk(x = 5):
        return x + a

    x = 15
    print(funk())

    car = Car('blue', 123123)
    print(car)

    lista_parzysta = [x for x in range(101) if x%2 == 0]
    print(lista_parzysta)

    # najczesciej występujący element listy:
    lista2 = [1, 1, 2, 2, 2, 3, 3, 3, 4, 1, 2, 4, 5, 1,2, 2, 2, 3]
    print("najczęściej występuje: ")
    print(max(lista2, key = lista2.count))


    print("filter")
    print(list(filter(lambda x: x%2 == 0, range(100))))
          
