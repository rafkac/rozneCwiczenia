# -*- coding: utf-8 -*-
# RafKac

import unittest
from unittest import TestCase

from calculator import Developer, Language, calculate
from main import fizzbuzz


class TestsForFizzbuzz(TestCase):
    def test_count_is_instance(self):
        count = "123qwe234"
        # za pomocą keyword arguments - czyli assertRaises(exception, callable, *args, **kwargs)
        self.assertRaises(TypeError, fizzbuzz, count=count)
        # za pomocą context manager - czyli assertRaises(exception, msg=None)
        with self.assertRaises(TypeError) as context:
            fizzbuzz(count=count)

    def test_count_is_between_1_and_1000(self):
        count = 15
        table = ['1', '2', 'Fizz', '4', 'Buzz', 'Fizz', '7', '8', 'Fizz', 'Buzz', '11', 'Fizz', '13', '14', 'FizzBuzz']
        self.assertEquals(fizzbuzz(count), table)

    def test_count_is_large_then_1000(self):
        count = 999999
        self.assertRaises(ValueError, fizzbuzz, count=count)

    def test_count_is_smaller_then_1(self):
        count = 0
        self.assertRaises(ValueError, fizzbuzz, count=count)

    def test_count_is_ok(self):
        count = 3
        table = ['1', '2', 'Fizz']
        self.assertIs(count, 3)
        self.assertEqual(fizzbuzz(count), table)


class TestsDeveloper(TestCase):
    def test_init_developer(self):
        dev = Developer(3, 4, {Language.SCALA, Language.C})

        assert dev != []
        assert dev.languages == {Language.SCALA, Language.C}
        assert dev.followers == 4
        assert dev.accepted_contributions == 3

    def test_bad_data(self):
        dev = Developer(3, 4, {23, 'iuo'})

        assert dev != []
        assert dev.languages != {Language.PYTHON}
        assert dev.languages is not None

        self.assertRaises(TypeError, dev, languages={23, 'iuo'})


class TestsCalculator(TestCase):
    def test_calculate_bad_data(self):
        dev = Developer(3, 4, {"uuuu", "ogogo"})
        print(calculate(dev))
    #    self.assertRaises(NotImplemented, calculate, dev=dev)

    def test_calculate_max_data(self):
        dev = Developer(3, 4, {Language.SCALA, Language.C, Language.PYTHON, Language.JAVA})
        calc = calculate(dev)
        assert calc == 72

    def test_calculate_min_data(self):
        languages = {Language.C}
        dev = Developer(3, 4, languages)

        self.assertNotIn(Language.PYTHON, languages)
        self.assertEqual(calculate(dev), 10)

    def test_calculate_two_with_python(self):
        languages = {Language.SCALA, Language.C, Language.PYTHON}
        dev = Developer(3, 4, languages)

        self.assertIn(Language.PYTHON, languages)
        self.assertEqual(calculate(dev), 44)

    def test_calculate_only_python(self):
        languages = {Language.PYTHON}
        dev = Developer(3, 4, languages)

        self.assertIn(Language.PYTHON, languages)
        self.assertEqual(calculate(dev), 12)

    def test_calculate_only_python_without_followers(self):
        languages = {Language.PYTHON}
        followers = 0
        dev = Developer(0, followers, languages)

        self.assertIn(Language.PYTHON, languages)
        self.assertEqual(calculate(dev), 4)

    def test_calculate_without_python_or_followers(self):
        languages = {Language.C}
        followers = 0
        dev = Developer(0, followers, languages)

        self.assertNotIn(Language.PYTHON, languages)
        self.assertEqual(calculate(dev), 2)

    def test_calculate_without_languages_or_followers(self):
        languages = {}
        followers = 0
        dev = Developer(0, followers, languages)

        self.assertNotIn(Language.PYTHON, languages)
        self.assertEqual(calculate(dev), 0)


if __name__ == "__main__":
    unittest.main()
