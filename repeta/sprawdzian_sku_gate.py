# -*- coding: utf-8 -*-
# RafKac

def pierwsze():
    slownik = {
        'ala': 'alo',
        2: 'halo',
        (1, 2): 'witam',
        (1, 3): 'co tam',
    }
    for slowo in slownik:
        print(slowo)


if __name__ == "__main__":
    pierwsze()
