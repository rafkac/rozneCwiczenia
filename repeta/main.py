# -*- coding: utf-8 -*-
# RafKac
# 1 - generator - metoda split(string, separator) jako generator podciągów
# 2 - generator - korzystanie z zewnętrznego API: https://php-quotes.herokuapp.com/api/quotes/1
# 3 - testy do metody fizzbuzz(count)
from urllib import request
from urllib.error import HTTPError
import json


def split(string, separator):
    """
    1 - generator
    """
    begin = 0
    end = 0
    while True:
        end = string.find(separator, begin+1)
        if end == -1:
            yield string[begin:]
            break
        else:
            yield string[begin: end]
            begin = end


def quota_from_api():
    """
    2 - generator
    """
    indyk = 1
    while True:
        url = "https://php-quotes.herokuapp.com/api/quotes/" + str(indyk)
        try:
            response = request.urlopen(url)
        except HTTPError:
            print(HTTPError.code)
            print(HTTPError.read())
            yield "HTTPError"
            break
        if response.status == 200:
            page = response.read()
            data = json.loads(page)
            # print(data["quoteAuthor"] + " - " + data["quoteText"])
            yield str(data["id"]) + " : " + data["quoteAuthor"] + " - " + data["quoteText"]
            indyk += 1


def fizzbuzz(count):
    """
    3 - testy
    """
    if not isinstance(count, int):
        raise TypeError('count must be an integer')
    if count < 1 or count > 1000:
        raise ValueError('count must be between 1 and 1000')
    results = []
    for num in range(1, count + 1):
        if num % 3 == 0 and num % 5 == 0:
            results.append('FizzBuzz')
        elif num % 3 == 0:
            results.append('Fizz')
        elif num % 5 == 0:
            results.append('Buzz')
        else:
            results.append(str(num))
    return results


def main():
    word = "volenti non fit iniuria, dura lex, sed lex."
    generator = split(word, " ")
    cif = word.count(" ")
    i = 0
    while True:
        print(next(generator))
        i += 1
        if i == cif:
            print()
            break

    gen2 = quota_from_api()
    i = 0
    while True:
        print(next(gen2))
        i += 1
        if i >= 10:
            break

    print("Fizzbuzz: ")
    print(fizzbuzz(15))


if __name__ == '__main__':
    main()
