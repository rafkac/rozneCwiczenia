# -*- coding: utf-8 -*-
# RafKac
from django.contrib.auth.models import User
from django.db import models


class Entry(models.Model):
    title = models.CharField('Title', max_length=100)
    image = models.ImageField(upload_to='entries/', null=True)
    pub_date = models.DateTimeField('Opublikowano')
    content = models.TextField('Tekst')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
