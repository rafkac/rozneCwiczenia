from django.apps import AppConfig


class NaszsystemConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'naszSystem'
