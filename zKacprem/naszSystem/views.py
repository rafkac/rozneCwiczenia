# -*- coding: utf-8 -*-
# RafKac

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm

from .forms import MySignupForm


def register(request):
    if request.method == 'POST':
        form = MySignupForm(request.POST)
        # print("request: {}".format(request.POST))
        if form.is_valid():
            user = form.save()

    form = MySignupForm()
    return render(
        request=request,
        template_name='naszSystem/register.html',
        context={
            'form': form,
        }
    )


@login_required
def panel(request):
    """
    Jest dostępny tylko wtedy, gdy user jest zalogowany.
    """
    return render(
        request=request,
        template_name='naszSystem/panel.html'
    )


def login(request):
    if request.method == 'POST':
        print("login - POST")
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            print("form - is valid")
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('username')
            print(username, password)
            user = authenticate(username=username, password=password)
            print(user)
            if login is not None:
                login(request, user)

    form = AuthenticationForm()

    return render(
        request=request,
        template_name='naszSystem/login.html',
        context={
            'form': form,
        }
    )
