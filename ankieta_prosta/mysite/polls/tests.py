# -*- coding: utf-8 -*-

import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import Question


class QuestionModelsTests(TestCase):
    def test_was_published_recently_with_future_question(self):
        """zwraca False dla pytań, których data publikacji jest w przyszłości"""
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)

        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        """zwraca False dla pytań, któych pub_date jest starsza niż 1 dzień"""
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)

        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        """zwraca True dla pytań, któch pub_date jest z ostatniego dnia"""
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)

        self.assertIs(recent_question.was_published_recently(), True)


def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)

    return Question.objects.create(question_text, pub_date=time)


class QuestionIndexViewTests(TestCase):
    def test_no_questions(self):
        """Jeśli nie ma pytań, to odpowiednia wiadomość jest wyświetlana"""
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")

        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        """pytania z przeszłą datą pub_date są wyświetlane na stronie index"""
        question = create_question(question_text="Past question.", days=-30)
        response = self.client.get(reverse('polls:index'))

        self.assertQuerysetEqual(response.context['latest_question_list'], [question], )

    def test_future_question(self):
        """Pytania z przyszłą datąpub_date nie będą wyświetlane na stronie index.html"""
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, "No polls are available.")

        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_future_question_and_past_question(self):
        """Jeśli istnieją przeszłe i przyszłe pytania, tylko przeszłe są wyświetlane"""
        question = create_question(question_text="Past question.", days=-30)
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse('polls:index'))

        self.assertQuerysetEqual(response.context['latest_question_list'], [question], )

    def test_two_past_question(self):
        """index może wyświetlać wiele pytań"""
        question1 = create_question(question_text="Past question 1", days=-30)
        question2 = create_question(question_text="Past question 2", days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], [question1, question2],)


class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        """widok szczegółów pytań z przyszłą pub_date zwraca 404 not found"""
        future_question = create_question(question_text="Future question.", days=5)
        url = reverse('polls:detail', args=(future_question.id, ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """widok szczegółów pytania z przeszłą pub_date wyświetli treść pytania."""
        past_question = create_question(question_text="Past Question.", days=-5)
        url = reverse('polls:detail', args=(past_question.id, ))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)


